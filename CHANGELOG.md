# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.3.0] - 2024-12-04

- Conservative Rate Limits added to celery
- AA v4

## [0.2.0] - 2024-05-11

- Drop Alpha status
- Drop unneeded dependencies
- Move from manual singleton to Django-Solo

## [0.1.6a] - 2023-10-11

- Retag for deployment

## [0.1.5a] - 2023-10-08

- Maintenance release, PEP621, AA4.x alpha, Py312
- Removes ESI endpoint offline warning
- Adds filter_horizontal to admin alumni config

## [0.1.4a] - 2023-04-06

- Maintenance release
- Adds PyPy testing
- Pre-Commit and formatting updates
- Adds a Django Check (Info) to warn on the esi endpoint being offline

## [0.1.2a] - 2022-07-09

- Re-issue of 0.1.1a to fix gitlab-ci

## [0.1.1a] - 2022-07-09

### Fixed

- Now correctly evaluates Character Corp Histories for corporation alumnis
- Correctly evaluates early when a user is _currently_ in an alumni entity

## [0.1.0a] - 2021-12-29

Initiate Versioning.

This alpha redoes our internal logic, we use corporation and alliance IDs to avoid bloating our models

### Added

### Changed

### Fixed
