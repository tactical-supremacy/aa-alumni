��          L      |       �      �   s   �   L   +  H   x  (   �  k  �     V  �   k  Q     M   i  *   �                                         Alumni Config An incrementing ID that can be used to canonically establish order of records in cases where dates may be ambiguous Characters with these Alliances in their History will be given Alumni Status Characters with these Corps in their History will be given Alumni Status True if the corporation has been deleted Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-05-11 10:42+0000
Last-Translator: Peter Pfeufer, 2024
Language-Team: German (https://app.transifex.com/alliance-auth/teams/107430/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Alumni-Konfiguration Eine aufsteigende ID, die zur kanonischen Festlegung der Reihenfolge von Datensätzen in Fällen verwendet werden kann, in denen Daten möglicherweise nicht eindeutig sind Charaktere mit diesen Allianzen in ihrer Vergangenheit erhalten den Alumni-Status Charaktere mit diesen Corps in ihrer Vergangenheit erhalten den Alumni-Status Wahr, wenn die Corporation gelöscht wurde 