"""
Integration with Alliance Auths State System, creates and maintains an Alumni State for past members of an Alliance and/or Corporation
"""
__version__ = "0.3.0"
__title__ = "Alumni"
